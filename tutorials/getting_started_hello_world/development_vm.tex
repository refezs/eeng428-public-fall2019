

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Creating Development VM Instance}

This section covers steps needed to create a {\tt c4.4xlarge} instance
that will be used to synthesize the hardware design (and optionally in future compile C code for
the software).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Create {\tt c4.4xlarge} Instance}

The development, compilation and synthesis of the C code and FPGA code is done using a virtual machine (VM)
running on a {\tt c4.4xlarge} instance in Amazon's cloud computing data centers.
To create a development VM for working with C and FPGA code, sign into the AWS console: \url{https://console.aws.amazon.com}.
In the top-right corner make sure you're in {\tt US East (N. Virginia)} region, Amazon EC2 F1 compute instances 
are not available in all regions\footnote{Also, selecting instance in region which is physically close to your location
should reduce network delays and improve connection speed.}.

From AWS console go to the {\tt EC2 Dashboard} and then go to {\tt Instances} page.  Now you can select {\tt Launch Instance} and begin the process
of configuring and launching an Amazon EC2 F1 compute instance.  Note, after each step press {\tt Next} until you get to Step 7,
do not skip steps and do not press {\tt Review and Launch} before Step 7.

\begin{itemize}
\item Step 1 -- Chose AMI: Search for ``FPGA'' in the AWS Marketplace and select the ``FPGA Developer AMI.''
This AMI contains all the licensed software needed to synthesize the FPGA code.
\item Step 2 -- Choose Instance Type: The recommended instance type is {\tt c4.4xlarge} for synthesizing your FPGA logic, also called Custom Logic (CL) by Amazon.
Other instance types can be used, but CPU, memory, and disk type will affect synthesis speed.
\item Step 3 -- Configure Instance: Default configuration should be sufficient, but ensure you are only launching 1 instance.
\item Step 4 -- Add Storage: Change the size of the root volume to 128 GiB, to ensure sufficient space to download and synthesize code.
\item Step 5 -- Add Tags: No actions needed in this step.
\item Step 6 -- Configure Security Group: No actions needed in this step.  However, ensure only port 22 (SSH) is open.
\item Step 7 -- Review Instance Launch: Press {\tt Launch} button to begin launching the instance.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Selecting Key Pair and Launch the Instance}

Instead of passwords, the virtual machines (VMs) in Amazon's cloud deployments
use cryptographic keys for authenticating users and letting you log into the virtual machine
via SSH connection.  A cryptographic key pair consists of a public key that AWS stores, and a private key that you store (in a file). 
Together, they allow you to connect to your instance securely.

If you don't have an existing key file, or if the file is lost, select {\tt Create a new key pair}
in the {\tt Select new key pair or create a new key pair} window.
Then, enter a name for the key and select {\tt Download Key Pair}
to download the key (as a {\tt .pem} file) to your computer.  After downloading the file, ensure to change permissions so only you, 
and not other users on your computer, can access the file.  Anybody with access to the file, or copy of the file, can log into your virtual machines.  The file
needs to be protected.  On Linux, you can use command: {\tt chmod 400 *.pem}.

Finally, press {\tt Launch Instances} to start the virtual machine.  You can monitor the status of the instance on the {\tt EC2 Dashboard} on the {\tt Instances} page.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Connecting to the Instance}

Once the VM has started and its status has changed to {\tt running} on the {\tt EC2 Dashboard} on the {\tt Instances} page,
you can select the instance by clicking it's name under the {\tt Instance ID}.
On the {\tt Instance summary} page that opens up, check that the instance has
a Public IPv4 address.

If it does not, you will need to create and assign an Elastic IP address to the instance.
Go into the {\tt EC2 Dashboard}, then in the {\tt Network \& Security} menu select {\tt Elastic IPs}.
Click on {\tt Allocate Elastic IP address}, in the address settings confirm you are in the right region,
and then click {\tt Allocate} on the bottom of the page.  Now in the {\tt Elastic IP addresses} table,
you should see a new address, select it, then click {\tt Actions} and then {\tt Associate Elastic IP address}.
You can now click the {\tt Instance} box to get a list of instances and select your instance.

On the {\tt Instance summary} page now the instance should have a
a Public IPv4 address.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{SSH'ing into the VM}

Now the VM status should still be {\tt running} on the {\tt EC2 Dashboard} under the {\tt Instances} page.
To SSH,
you can select the instance, then press {\tt Actions}
and then press {\tt Connect}.  Each time you start the VM, it will get a different IP address, so you need to check
the {\tt Connect} window to see the proper IP and command line to connect to the instance.  The {\tt Connect} window
gives information how to connect to the instance using SSH and the {\tt *.pem} file that contains your secret key.  

Note, for Linux
users, you are given command line you can cut-and-paste straight into terminal.  Windows (PuTTY) users need to follow
extra information that is given in the {\tt Connect} window.

Further, note that the user name you are using to connect to the VM is by default {\tt root}.
However, for the ``FPGA Developer AMI'' the proper user name is {\tt centos} (the VM uses CentOS Linux operating system distribution).
There should be no password associated with the {\tt centos} user, and the user has root privilege.
E.g. you can execute {\tt sudo} commands without password, such as
{\tt sudo yum install vim} to install software such as the Vim text editor.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Setting up AWS CLI and S3 Bucket to Enable AFI Creation}

As the developer you are required to create a S3 bucket for the AFI (Amazon FPGA Image) generation. 
A S3 bucket is similar to a file stored on a remote server, the files are not stored inside the VM instance,
rather they are stored remotely in Amazon's cloud.
The bucket will contain a {\tt *.tar} file and logs which are generated from the AFI creation service.
To be able to create S3 buckets from command line, and to do other AWS related operations from the command line, the AWS CLI has to be first configured.

\subsubsection{Setup AWS CLI Access}

AWS CLI (Command Line Interface) tools need to know your credentials so that when you execute AWS CLI commands.
You can configure the AWS CLI using below command:

\begin{verbatim}
    $ aws configure
\end{verbatim}

You will need your security credentials.  Go to \url{https://console.aws.amazon.com/iam/home?#/security_credential}.
You may need to select {\tt Continue to Security Credentials} in a pop-up window.
Expand the {\tt Access keys (access key ID and secret access key)} tab.
Click {\tt Create New Access Key}.
Make sure to download the access key file, you will not have access to the secret key after you finish the access key creation process.
The file will be a {\tt *.csv} (comma separated values file type) which can be opened with most spreadsheet programs or text editors.
Save the file for future access.

In the {\tt aws configure} command, you will need to enter your access key and the corresponding secret key.
Also, make sure to set the region to {\tt us-east-1} and select output format as {\tt json}.  
Note that the input text is are case-sensitive, e.g.  {\tt us-east-1} and not  {\tt US-East-1}
You can re-run the command if you make any mistakes.

\subsubsection{Create S3 Bucket}

This S3 bucket will be used by the AWS SDAccel scripts to upload your DCP (digital checkpoint)
to AWS for AFI generation.
Start by creating a bucket and a folder within your new bucket, details about the commands are below.

The first command creates an S3 bucket, which needs to have a unique bucket name among all of buckets in S3.
The file name cannot have upper-case letters or underscores.
Replace the $<$bucket-name$>$ with the actual bucket name you want to use.  Do not use $<$ or $>$ in the actual name.
The name can only use lower-case letters and dashes.
The name needs to be globally unique, so pre-pending the name with ``eeng428'' can help you
have a useful, but unique name for your bucket.

\begin{verbatim}
    $ aws s3 mb s3://<bucket-name> --region us-east-1
    $ aws s3 mb s3://<bucket-name>/<dcp-folder-name>
    $ touch FILES_GO_HERE.txt
    $ aws s3 cp FILES_GO_HERE.txt s3://<bucket-name>/<dcp-folder-name>/
\end{verbatim}

The second command creates a folder within the bucket.
Replace the $<$dcp-folder-name$>$ with the actual folder name.
Again, do not use $<$ or $>$ in the actual name of the folder.

The third command simply creates a blank file in the local directory, not in bucket, yet.
The fourth command copies the blank file into the DCP folder in the bucket, which forces
the bucket's folders to be actually created.
Don't forget the trailing slash at end of last command.

The AFI creation process will also generate logs that will be placed into your S3 bucket. 
These logs can be used for debug if the AFI generation fails.
Thus, create a folder for your log files:

\begin{verbatim}
    $ aws s3 mb s3://<bucket-name>/<logs-folder-name>
    $ touch LOGS_FILES_GO_HERE.txt
    $ aws s3 cp LOGS_FILES_GO_HERE.txt s3://<bucket-name>/<logs-folder-name>/
\end{verbatim}

The first command creates a folder to keep your logs.
The second command creates a blank file again.
The third command copies blank file into the bucket and trigger creation of the folder on S3.

The same bucket can be re-used among projects or as you re-synthesize the design, e.g.
create different DCP sub-folders for each project.
Alternatively, you can create separate buckets for each project to keep things organized.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Installing the HDK and Setting up the Environment}

The hardware development kit (HDK) contains all the scripts and links to tools needed to synthesize your Custom Logic
designed in VHDL or Verilog into the digital checkpoint (DCP) that will be used to finally create the Amazon FPGA Image
(AFI) that can be used to configure the FPGA.

\subsubsection{Get and Setup Amazon AWS FPGA Code}

First, {\tt git clone} a copy of aws-fpga repository in the home directory for the VM.

\begin{verbatim}
    $ git clone https://github.com/aws/aws-fpga
\end{verbatim}

Next, enter the aws-fpga directory and source hdk\_set.sh.

\begin{verbatim}
    $ cd aws-fpga/
    $ source hdk_setup.sh
\end{verbatim}

Sourcing hdk\_setup.sh will set required environment variables that are used throughout the examples in the HDK.
Optionally, source software development kit (SDK) setup script to get access to scripts about AFI's 
created or scripts to get e-mails for AFI generation updates.

\begin{verbatim}
    $ source sdk_setup.sh
\end{verbatim}

You may need to source the HDK and SDK scripts each time you log into the VM.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Select Hello World Example for Synthesis}

Select the Hello World example to be your Custom Logic directory and set your email address
to receive notification.  Note, sometimes the notification by e-mail does not work, usually if there is some
error in compilation or environment is not setup correctly.  Always check manually if the compilation is done.

\begin{verbatim}
    $ export HDK_DIR=$(pwd)/hdk
    $ cd $HDK_DIR/cl/examples/cl_hello_world    
    $ export CL_DIR=$(pwd)
    $ export EMAIL=your.email@example.com
\end{verbatim}

Setting up the CL\_DIR environment variable is crucial as the build scripts rely on that value to locate
the code that should be synthesized.
Each CL\_DIR follows the recommended directory structure to match the expected structure for HDK simulation and build scripts. 
The EMAIL environment variable is used to notify you when your Custom Logic build has completed.

Note, first time you synthesize a design and request an e-mail, you may get an e-mail
from Amazon about {\em AWS Notification - Subscription Confirmation}.  You need to
confirm your subscription to the notification e-mails before you will actually be able to receive them.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Start the Build Process for the Custom Logic}

Executing the {\tt aws\_build\_dcp\_from\_cl.sh}
script will perform the entire synthesis process converting the CL design into a completed Design Checkpoint
that meets timing and placement constrains of the target FPGA. The output is a tarball file comprising the DCP file, and other log/manifest files, 
formatted as {\tt YY\_MM\_DD-hhmm.Developer\_CL.tar}.
This file would be submitted to AWS to create an AFI. 
By default the build script will use Clock Group A Recipe A0 which uses a main clock of 125 MHz.
I.e. your custom logic will be running at 125 MHz.

Start the build process:

\begin{verbatim}
    $ cd $CL_DIR/build/scripts
    $ ./aws_build_dcp_from_cl.sh -notify
\end{verbatim}

Once the build process is started, you may disconnect from the VM (e.g. end the SSH session), and re-connect later
to check on the build process or to continue with the generation of the AFI image if the build finished successfully.  Note, however,
the VM cannot be stopped so that the compilation process continues.
Also note, the design may take few hours to synthesize -- once done, the VM can be stopped if you don't want to immediately
proceed with the AFI creating steps.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Monitoring Build Process}

Since the DCP generation can take up to several hours to complete, hence the {\tt aws\_build\_dcp\_from\_cl.sh}
will run the main build process (vivado) in within a {\tt nohup} context:
This will allow the build to continue running even if the SSH session is terminated half way through the run, for example.
The {\tt -notify} flag indicates to the script to notify you at your EMAIL address when the build is completed.

If you do not receive the notification e-mail, check if the {\tt *.tar} file is generated in {\tt \$CL\_DIR/build/checkpoints/to\_aws} directory.

If the compilation fails, no {\tt *.tar} file will be generated.
You can find logs of the most recent compilation in the {\tt \$CL\_DIR/build/scripts} folder.
You may need to read the log files to find the source of the error.

When build process is done successfully, the final {\tt *.tar} file should be available in sub directory of CL\_DIR:

\begin{verbatim}
    $ cd $CL_DIR/build/checkpoints/to_aws
\end{verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Submit the DCP Tarball to AWS to Create the AFI}

Once you have the {\tt *.Developer\_CL.tar}, which contains the DCP (Design Checkpoint), you can being AFI creation.
To submit the DCP, use the S3 bucket and upload the tarball file into that bucket. 
You need to prepare the following information:

\begin{enumerate}
\item Name of the logic design (Optional).
\item Generic description of the logic design (Optional).
\item Location of the tarball file within S3 bucket.
\item Location of an S3 directory where AWS would write back logs of the AFI creation.
\item AWS region where the AFI will be created. 
An AFI needs to be created in same region as the F1 instance that will use it.
Use {\tt copy-fpga-image} AWS CLI command to copy an AFI to a different region if needed.
\end{enumerate}

To upload your tarball file to S3,
use the bucket and folder you created initially for your tarball, then copy files into S3
using a single command:

\begin{verbatim}
    $ aws s3 cp $CL_DIR/build/checkpoints/to_aws/*.Developer_CL.tar \
      s3://<bucket-name>/<dcp-folder-name>/
\end{verbatim}

Note, the trailing '/' is required after {\tt $<$dcp-folder-name$>$}.
Also, if you have logged out (while the design synthesizes) you may need to {\tt export} the
\$CL\_DIR variable again, as the environment variables are reset each time you log back in.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Start AFI Creation}

Use the {\tt create-fpga-image} command to send the tarball file to Amazon for AFI creation.

\begin{verbatim}
    $ aws ec2 create-fpga-image \
        --region <region> \
        --name <afi-name> \
        --description <afi-description> \
        --input-storage-location Bucket=<dcp-bucket-name>,Key=<path-to-tarball> \
        --logs-storage-location Bucket=<logs-bucket-name>,Key=<path-to-logs> \
        [ --client-token <value> ] \
        [ --dry-run | --no-dry-run ]
\end{verbatim}


Note, {\tt $<$path-to-tarball$>$} is your {\tt $<$dcp-folder-name$>$/$<$tar-file-name$>$}, while {\tt $<$path-to-logs$>$} is your {\tt $<$logs-folder-name$>$}.

The output of this command includes two identifiers that refer to your AFI:

\begin{itemize}

\item FPGA Image Identifier or AFI ID: this is the main ID used to manage your AFI through 
the AWS CLI commands and AWS SDK APIs. This ID is regional, i.e., if an AFI is copied across multiple regions, 
it will have a different unique AFI ID in each region. An example AFI ID is afi-06d0ffc989feeea2a.

\item Global FPGA Image Identifier or AGFI ID: this is a global ID that is used to refer to an AFI from within an F1 instance. 
For example, to load or clear an AFI from an FPGA slot, you use the AGFI ID. Since the AGFI IDs is global (by design), 
it allows you to copy a combination of AFI/AMI to multiple regions, and they will work without requiring any extra setup. 
An example AGFI ID is agfi-0f0e045f919413242.

\end{itemize}

The {\tt create-fpga-image} command submits your Custom Logic to Amazon for approval. This usually takes about 20-30 minutes.

The {\tt describe-fpga-images} API allows you to check the AFI state during the background AFI generation process. 
You must provide the FPGA Image Identifier returned by {\tt create-fpga-image}:

\begin{verbatim}
    $ aws ec2 describe-fpga-images --fpga-image-ids afi-06d0ffc989feeea2a
\end{verbatim}

You can use the {\tt wait\_for\_afi.py} script to wait for the AFI creation to complete and get an email with the results.
The AFI can only be loaded to an instance once the AFI generation completes and the AFI state is set to {\tt available}.

