// top-level testbench module for the board with clock,
// counter and the seven segment decoder
module boardWithConcatenation;

  wire clock, eSeg, w3, w2, w1, w0;
  wire [3:0] countVal;

  assign countVal = {w3, w2, w1, w0};

  // sub modules
  m16 counter ({w3, w2, w1, w0}, clock);
  m555 clockGen (clock);
  binaryToESeg disp (eSeg, w3, w2, w1, w0);

  initial
  begin
    $monitor ($time,,,"count=%d, eSeg=%d", countVal, eSeg);
    #200 $finish;
  end

  initial
  begin
    // dump signal traces into a VCD file
    $dumpfile("example_1-8_to_1-11_waveform.vcd");
    $dumpvars(0,boardWithConcatenation);
  end

endmodule

// counter module
module m16
( output reg [3:0] ctr = 1,
  input clock
);

  // counter ctr is initialized to 1 when devined,
  // then awlays count up on clock edge
  always @(posedge clock)
  begin
    ctr <= ctr + 1;
  end

endmodule

// clock module, named after the m555 timer chip
module m555
( output reg clock
);

  // initialize clock at 5th cycle of simulator
  initial
  begin
    #5 clock = 1;
  end

  // every 50 cycles of simulation, toggle the clock
  always
  begin
    #50 clock = ~ clock;
  end

endmodule

// seven segment decoder for segment e
module binaryToESeg
( output eSeg,
  input A, B, C, D
);

  wire p1, p2, p3, p4;

  nand #1
    g1 (p1, C, ~D),
    g2 (p2, A, B),
    g3 (p3, ~B, ~D),
    g4 (p4, A, C),
    g5 (eSeg, p1, p2, p3, p4);

endmodule
