// top-level testbench module for the seven segment decoder
module testBench;

  wire w1, w2, w3, w4, w5;

  binaryToESeg d (w1, w2, w3, w4, w5);
  test_bToESeg t (w1, w2, w3, w4, w5);

endmodule

// design under test (DUT): seven segment display driver for segment e
module binaryToESeg
( input A, B, C, D,
  output eSeg
);

  wire p1, p2, p3, p4;

  nand #1
    g1 (p1, C, ~D),
    g2 (p2, A, B),
    g3 (p3, ~B, ~D),
    g4 (p4, A, C),
    g5 (eSeg, p1, p2, p3, p4);

endmodule

// tester unit: drive inputs to DUT
module test_bToESeg
( output reg A, B, C, D,
  input eSeg
);

  initial
  begin
    $monitor ($time,,
    "A = %b B = %b C = %b D = %b, eSeg = %b",
     A, B, C, D, eSeg);

    // simulate input changes to drive the design under test
    #10 A = 0; B = 0; C = 0; D = 0;
    #10 D = 1;
    #10 C = 1; D = 0;
    #10 $finish;
  end

  initial
  begin
    // dump signal traces into a VCD file
    $dumpfile("example_1-4_waveform.vcd");
    $dumpvars(0,testBench);
  end

endmodule
